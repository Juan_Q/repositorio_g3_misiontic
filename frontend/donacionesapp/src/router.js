import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import LogIn from "./components/LogIn.vue";
import SignUp from "./components/SignUp.vue";
import Admin from "./components/Admin.vue";
import Donaciones from "./components/Admin/Donaciones.vue";
import Emergencias from "./components/Admin/Emergencias.vue";
import MetodosPago from "./components/Admin/MetodosPago.vue";

const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/user/logIn",
    name: "logIn",
    component: LogIn,
  },
  {
    path: "/user/signUp",
    name: "signUp",
    component: SignUp,
  },
  {
    path: "/user/admin",
    name: "admin",
    component: Admin,
  },
  {
    path: "/user/Admin/donaciones",
    name: "donaciones",
    component: Donaciones,
  },
  {
    path: "/user/Admin/emergencias",
    name: "emergencias",
    component: Emergencias,
  },
  {
    path: "/user/Admin/metodosPago",
    name: "metodosPago",
    component: MetodosPago,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
