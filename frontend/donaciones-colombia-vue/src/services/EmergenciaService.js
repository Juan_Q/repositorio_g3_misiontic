import axios from 'axios'

// Una constante que almacena la instancia de Axios
const apiClient = axios.create({
    baseURL: 'https://back-donaciones-co.herokuapp.com/api/',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})



export default {

    // Método que nos obtiene la información del backend
    getEvents() {
        return apiClient.get('/emergenciaDonaciones/')
    },
}
