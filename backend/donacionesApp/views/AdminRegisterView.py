from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from donacionesApp.serializers.AdminSerializer import AdminSerializer


class AdminRegisterView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = AdminSerializer(data=request.data)
        # Valida que los tipos de datos sean correctos con relación al modelo
        serializer.is_valid(raise_exception=True)
        # Llama al create del serializer
        serializer.save()
        # Creación de diccionario con la información para guardar el token
        tokenData = {
            # Obtiene los valores del body del JSON
            "username": request.data["username"],
            "password": request.data["password"]
        }
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        return Response(tokenSerializer.validated_data,
                        status=status.HTTP_201_CREATED)
