from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from donacionesApp.models.AdminModel import Administrador
from donacionesApp.serializers.AdminSerializer import AdminSerializer


class AdminDetailView(generics.RetrieveAPIView):
    queryset = Administrador.objects.all()
    # atributo de la clase
    serializer_class = AdminSerializer
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        # Guardamos el token que recibimos de la petición del Front end
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        # Validar que el tocken corresponda a algo que se haya generado aca
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        # Decodificar el tocken y verificar si fue un tocken que se genero con nuestro algoritmo
        # valid_data guarda también el usuario para quien se genero ese tocken en user_id
        valid_data = tokenBackend.decode(token, verify=False)
        # Verificación que la información del usuario que se esta solicitando (pk) es el mismo usuario que tiene
        # permiso en el tocken
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail': 'Unauthorized Request'}
            return Response(stringResponse,
                            status=status.HTTP_401_UNAUTHORIZED)
        # la calse madre se encarga de hacer el query, invocar al serializer y returnar el JSON
        return super().get(request, *args, **kwargs)