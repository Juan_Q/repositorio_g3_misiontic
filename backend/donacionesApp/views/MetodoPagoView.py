from django.http import response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from donacionesApp.serializers.MetodoPagoSerializer import MetodoPagoSerializer
from donacionesApp.models.MetodoPagoModel import MetodoPago


@api_view(['GET','POST'])
def Metodo_Pago(request):
    #Mostrar
    if request.method == 'GET':
        pago = MetodoPago.objects.all()
        pago_serializer = MetodoPagoSerializer(pago, many=True)
        print(type(pago_serializer))
        return Response(pago_serializer.data, status = status.HTTP_200_OK)
    
    #Crear
    elif request.method == 'POST':
        pago_serializer = MetodoPagoSerializer(data = request.data)
        if pago_serializer.is_valid():
            pago_serializer.save()
            return Response(pago_serializer.data, status = status.HTTP_201_CREATED)
        return Response(pago_serializer.errors, status = status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT','DELETE'])
def Metodo_Pago_Pk(request, pk=None):
    #Consulta
    pago = MetodoPago.objects.filter(idMetodoPago = pk).first()
    
    #Validación
    if pago:
        
        #Mostrar
        if request.method == 'GET':
            pago_serializer = MetodoPagoSerializer(pago)
            return Response(pago_serializer.data, status = status.HTTP_200_OK)
        
        #Actualizar
        elif request.method == 'PUT':
            pago_serializer = MetodoPagoSerializer(pago, data = request.data, partial=True)
            if pago_serializer.is_valid():
                pago_serializer.save()
                return Response(pago_serializer.data, status = status.HTTP_200_OK)
            return Response(pago_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        
        #Eliminar
        elif request.method == 'DELETE':
            pago.delete()
            return Response('Registro Eliminado', status = status.HTTP_200_OK)
        
    return Response('No se encuentra el registro', status = status.HTTP_400_BAD_REQUEST)