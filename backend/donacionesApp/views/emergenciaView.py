from rest_framework.response import Response
from rest_framework.decorators import api_view
from donacionesApp.models.DonacionModel import Donacion
from donacionesApp.models.emergencia import Emergencia
from donacionesApp.models.DonacionModel import Donacion
from donacionesApp.serializers.emergenciaSerializer import EmergenciaSerializer, EmergenciaDonacionesSerializer, DonacionesSerializer


@api_view(['GET', 'POST'])
def emergencia_api_view(request):
    # Trabaja con una lista de registros

    if request.method == 'GET':
        emergencias = Emergencia.objects.all()
        # Many es un parámetro que le indica al serializer que debe serializar múltiples objetos
        emergencias_serializer = EmergenciaSerializer(emergencias, many=True)
        return Response(emergencias_serializer.data)

    elif request.method == 'POST':
        # El serializer valida que el JSON recibido es igual al objeto del modelo
        emergencia_serializer = EmergenciaSerializer(data=request.data)
        if emergencia_serializer.is_valid():
            # Registro en la BD
            emergencia_serializer.save()
            # Respondemos que sí se ejecuto correctamente el POST al retornar el archivo JSON
            # que se deserealizó
            return Response(emergencia_serializer.data)
        return Response(emergencia_serializer.errors)


@api_view(['GET', 'PUT', 'DELETE', 'PATCH'])
# Solo trabaja con un registro
def emergencia_detail_api_view(request, pk=None):

    # En request.data nos llega la información que envia el frontend
    if request.method == 'GET':
        #Obtener la instancia asociada al pk
        emergencia = Emergencia.objects.filter(id_emergencia=pk).first()
        emergencia_serializer = EmergenciaSerializer(emergencia)
        return Response(emergencia_serializer.data)

    elif request.method == 'PUT':
        emergencia = Emergencia.objects.filter(id_emergencia=pk).first()
        # El serializer interpreta ambos parametros como un update de la nueva data que va a emergencia
        emergencia_serializer = EmergenciaSerializer(emergencia,
                                                     data=request.data,
                                                     partial=True)
        if emergencia_serializer.is_valid():
            emergencia_serializer.save()
            return Response(emergencia_serializer.data)
        return Response(emergencia_serializer.errors)

    elif request.method == 'DELETE':
        emergencia = Emergencia.objects.filter(id_emergencia=pk).first()
        emergencia.delete()
        return Response('Eliminado')


@api_view(['GET'])
def emergencia_donaciones_api_view(request, pk=None):
    # Retorna la sumatoria de donaciones realizadas a una emergencia

    if request.method == 'GET':
        #Obtener la instancia asociada al pk
        emergencia = Emergencia.objects.filter(id_emergencia=pk).first()
        #print(emergencia.id_emergencia)
        emergencia_serializer = EmergenciaDonacionesSerializer(emergencia)
        return Response(emergencia_serializer.data)

@api_view(['GET'])
def emergencia_donaciones_detail_api_view(request):
    # Retorna la sumatoria de donaciones realizadas a una emergencia

    if request.method == 'GET':
        #Obtener la instancia asociada al pk
        emergencia = Emergencia.objects.all()
        emergencia_serializer = EmergenciaDonacionesSerializer(emergencia, many=True)
        return Response(emergencia_serializer.data)

@api_view(['GET'])
def donaciones_api_view(request):
    # Retorna la sumatoria de donaciones realizadas a una emergencia

    if request.method == 'GET':
        donacion = Donacion.objects.all()
        emergencia_serializer = DonacionesSerializer(donacion, many=True)
        return Response(emergencia_serializer.data)