from rest_framework.decorators import api_view
from rest_framework.response import Response
from donacionesApp.models.donantesModel import Donantes
from donacionesApp.serializers.donantesSerializer import DonantesSerializer


@api_view(['GET', 'POST'])
def donantes_api_view(request):

    if request.method == 'GET':
        donantes = Donantes.objects.all()
        #many es un parámetro que le indica al serializer que debe serializar múltiples objetos
        donantes_serializer = DonantesSerializer(donantes, many=True)
        return Response(donantes_serializer.data)

    elif request.method == 'POST':
        #el serializer valida que el JSON recibido es igual al objeto del modelo
        donantes_serializer = DonantesSerializer(data=request.data, many=True)
        if donantes_serializer.is_valid():
            #registro en la BD
            donantes_serializer.save()
            #respondemos que si se ejecuto correctamente el POST al retornar el archivo JSON
            #que se deserealizó
            return Response(donantes_serializer.data)
        return Response(donantes_serializer.errors)

@api_view(['GET', 'PUT', 'DELETE', 'PATCH'])
#solo retorna un registro
def donantes_detail_api_view(request, pk=None):
    #en request.data nos llega la información que envia el frontend
    if request.method == 'GET':
        #Obtener la instancia asociada al pk
        donantes = Donantes.objects.filter(idDonante=pk).first()
        donantes_serializer = DonantesSerializer(donantes)
        return Response(donantes_serializer.data)

    elif request.method == 'PUT':
        donantes = Donantes.objects.filter(idDonante=pk).first()
        #El serializer interpreta ambos parametros como un update de la nueva en data que va a donantes
        donantes_serializer = DonantesSerializer(Donantes,
                                                     data=request.data,
                                                     partial=True)
        if donantes_serializer.is_valid():
            donantes_serializer.save()
            return Response(donantes_serializer.data)
        return Response(donantes_serializer.errors)

    elif request.method == 'DELETE':
        donantes = Donantes.objects.filter(idDonante=pk).first()
        donantes.delete()
        return Response('Eliminado')