from rest_framework.response import Response
from rest_framework.decorators import api_view
from donacionesApp.models.DonacionModel import Donacion
from donacionesApp.serializers.DonacionSerializer import DonacionSerializer


@api_view(['GET', 'POST'])
def donacion_api_view(request):

    if request.method == 'GET':
        donaciones = Donacion.objects.all()
        donaciones_serializer = DonacionSerializer(donaciones, many=True)
        return Response(donaciones_serializer.data)

    elif request.method == 'POST':
        donaciones_serializer = DonacionSerializer(data=request.data)
        if donaciones_serializer.is_valid():
            donaciones_serializer.save()
            return Response(donaciones_serializer.data)
        return Response(donaciones_serializer.errors)
