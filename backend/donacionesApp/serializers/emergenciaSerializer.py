from rest_framework import serializers
from donacionesApp.models.MetodoPagoModel import MetodoPago
from donacionesApp.models.donantesModel import Donantes
from donacionesApp.models.emergencia import Emergencia
from donacionesApp.models.DonacionModel import Donacion
from django.db.models import Sum


class EmergenciaDonacionesSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):

        temp_values = [
            donacion.monto for donacion in Donacion.objects.filter(
                id_emergencia_fk=instance.id_emergencia)
        ]
        sumatoria = sum(temp_values)

        return {
            'idEmergencia': instance.id_emergencia,
            'nombreEmergencia': instance.nombre_emergencia,
            'departamento': instance.departamento,
            'municipio': instance.municipio,
            'fechaCreacion': instance.fecha_creacion,
            'fechaVencimiento': instance.fecha_vencimiento,
            'montoObjetivo': instance.monto_objetivo,
            'montoRecaudado': sumatoria
        }

class DonacionesSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return {
            'idDonacion': instance.id_donacion,
            'fechaDonación': instance.fecha_creacion,
            'monto': instance.monto,
            'metodoPago': instance.id_metodoPago_fk.metodoPago,
            'nombreEmergencia': instance.id_emergencia_fk.nombre_emergencia,
            'primerNombreDonante': instance.id_donante_fk.primerNombre,
            'segundoNombreDonante': instance.id_donante_fk.segundoNombre,
            'primerApellidoDonante': instance.id_donante_fk.primerApellido,
            'segundoApellidoDonante': instance.id_donante_fk.segundoApellido,
            'dirección': instance.id_donante_fk.direccion,
            'departamento': instance.id_donante_fk.departamento,
            'eMail': instance.id_donante_fk.eMail,
            'celular': instance.id_donante_fk.celular
        }
        
class EmergenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Emergencia
        fields = '__all__'
