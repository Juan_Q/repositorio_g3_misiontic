from rest_framework import serializers
from donacionesApp.models.DonacionModel import Donacion
from donacionesApp.serializers.DonacionSerializer import DonacionSerializer
from donacionesApp.models.donantesModel import Donantes

class DonantesSerializer(serializers.ModelSerializer):
    donacionDonante = DonacionSerializer()
    class Meta:
        model = Donantes
        fields = ['idDonante','primerNombre','segundoNombre','primerApellido','segundoApellido','direccion','departamento','eMail','celular','donacionDonante']
    
    def create(self, validated_data): 
        donacionDonanteData =validated_data.pop('donacionDonante')
        donacionInstance = Donantes.objects.create(**validated_data)
        Donacion.objects.create(id_donante_fk = donacionInstance, **donacionDonanteData)
        return donacionInstance