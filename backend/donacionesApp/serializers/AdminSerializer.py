from donacionesApp.models.AdminModel import Administrador
from rest_framework import serializers


class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administrador
        fields = '__all__'

    # validated_data es el json que recibimos convertido a diccionario. Definimos como convertirlo a un objeto
    # lo deserealizamos
    def create(self, validated_data):
        # Creación de un objeto administrador emparejando la información en validated_data con el modelo
        userInstance = Administrador.objects.create(**validated_data)
        return userInstance

    # obj es el objeto que creamos en create, aqui se serializa, se pasa a JSON
    def to_representation(self, obj):
        user = Administrador.objects.get(id=obj.id)
        return {'id': user.id, 'username': user.username}
