from donacionesApp.models.MetodoPagoModel import MetodoPago
from rest_framework import serializers

class MetodoPagoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetodoPago
        fields = '__all__'