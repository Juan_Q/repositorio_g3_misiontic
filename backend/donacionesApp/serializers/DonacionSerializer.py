from rest_framework import serializers
from donacionesApp.models.DonacionModel import Donacion

class DonacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Donacion
        fields = ['monto','id_emergencia_fk','id_metodoPago_fk']
        
    