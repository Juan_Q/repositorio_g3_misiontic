from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .views.emergenciaView import emergencia_api_view, emergencia_detail_api_view, emergencia_donaciones_api_view, emergencia_donaciones_detail_api_view, donaciones_api_view
from .views.MetodoPagoView import Metodo_Pago, Metodo_Pago_Pk
from .views.AdminDetailView import AdminDetailView
from .views.AdminRegisterView import AdminRegisterView
from .views.DonacionView import donacion_api_view
from .views.donantesView import donantes_api_view, donantes_detail_api_view

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('Admin/', AdminRegisterView.as_view()),
    path('Admin/<int:pk>/', AdminDetailView.as_view()),
    path('emergencia/', emergencia_api_view),
    path('emergencia/<int:pk>/', emergencia_detail_api_view),
    path('MetodoPago/', Metodo_Pago),
    path('MetodoPago/<int:pk>', Metodo_Pago_Pk),
    path('donacion/', donacion_api_view),
    path('emergenciaDonaciones/<int:pk>', emergencia_donaciones_api_view),
    path('emergenciaDonaciones/', emergencia_donaciones_detail_api_view),
    path('Donantes/', donantes_api_view),
    path('Donantes/<int:pk>', donantes_detail_api_view),
    path('donaciones/', donaciones_api_view),
]