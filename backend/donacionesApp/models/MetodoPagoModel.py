from django.db import models

class MetodoPago(models.Model):
    idMetodoPago = models.BigAutoField(primary_key = True)
    metodoPago = models.CharField('metodoPago', max_length = 20, unique = True)
