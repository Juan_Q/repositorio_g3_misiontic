from django.db import models


class Emergencia(models.Model):

    id_emergencia = models.BigAutoField(primary_key=True)
    nombre_emergencia = models.CharField("nombre emergencia", max_length=30)
    monto_objetivo = models.FloatField()
    municipio = models.CharField("municipio", max_length=30)
    departamento = models.CharField("departamento", max_length=30)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_vencimiento = models.DateField()
