from django.db import models

class Donantes(models.Model):

    idDonante= models.BigAutoField(primary_key=True)
    primerNombre = models.CharField("Primer nombre", max_length=30)
    segundoNombre = models.CharField("Segundo nombre", max_length=30)
    primerApellido = models.CharField("Primer apellido", max_length=30)
    segundoApellido= models.CharField("Segundo apellido", max_length=30)
    direccion = models.CharField("Direccion", max_length=15)
    departamento = models.CharField("Departamento", max_length=15)
    eMail = models.CharField("eMail", max_length=30)
    celular = models.CharField("Numero de celular", max_length=10)
