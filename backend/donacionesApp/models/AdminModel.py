from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password


class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class Administrador(AbstractBaseUser, PermissionsMixin):
    # El Big es una forma en postgrest de crear autogenerar primary key autogeneradas de 64 caracteres
    # aleatorios, lo que las hace más seguras.
    id = models.BigAutoField(primary_key=True)
    username = models.CharField('Username', max_length=15, unique=True)
    password = models.CharField('Password', max_length=256)

    def save(self, **kwargs):
        # some_salt tiene un texto que nos ayuda a cifrar la contraseña que recibimos.
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        # con base a la contraseña original y la palabra clave cifra y sobre escribe password a través
        # de una operación de hashing que luego es guardada en la base de datos.
        self.password = make_password(self.password, some_salt)
<<<<<<< HEAD
        print(self.password)
=======
        # Llama a AbstractBaseUser para guardar el usuario.
>>>>>>> 6e331d9e4a8e5a2f31ceb0941045338e9679670f
        super().save(**kwargs)

    # Indicamos cuál va a ser el manager del modelo
    objects = UserManager()
    USERNAME_FIELD = 'username'