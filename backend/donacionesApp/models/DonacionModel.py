from django.db import models
from .MetodoPagoModel import MetodoPago
from .donantesModel import Donantes
from .emergencia import Emergencia

class Donacion(models.Model):

    id_donacion = models.BigAutoField(primary_key=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    monto = models.FloatField()
    id_emergencia_fk = models.ForeignKey(Emergencia, related_name="donacionEmergencia", on_delete=models.CASCADE)
    id_donante_fk = models.ForeignKey(Donantes, related_name="donacionDonante", on_delete=models.CASCADE)
    id_metodoPago_fk = models.ForeignKey(MetodoPago, related_name="donacionMetodoPago", on_delete=models.CASCADE)