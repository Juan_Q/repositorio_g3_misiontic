from django.contrib import admin
from .models.emergencia import Emergencia
from .models.MetodoPagoModel import MetodoPago
from .models.AdminModel import Administrador
from .models.DonacionModel import Donacion
from .models.donantesModel import Donantes

# Register your models here.
admin.site.register(MetodoPago)
admin.site.register(Emergencia)
admin.site.register(Administrador)
admin.site.register(Donacion)
admin.site.register(Donantes)